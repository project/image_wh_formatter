<?php

/**
 * @file
 * Add a width and height formatter for image field
 */

/**
 * Implements hook_theme().
 */
function image_wh_formatter_theme() {
  return array(
    'image_wh_formatter' => array(
      'variables' => array(
        'item' => NULL,
        'image_style' => NULL,
        'property' => NULL,
        'property_suffix' => NULL,
        'property_separator' => NULL
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function image_wh_formatter_field_formatter_info() {
  $formatters = array(
    'image_wh' => array(
      'label' => t('Image Width'),
      'field types' => array('image', 'imagefield_crop'),
      'settings' => array(
        'image_style' => '',
        'property' => 'wh',
        'property_suffix' => ' px',
        'property_separator' => ' x '
      ),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function image_wh_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE);
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  $properties = array('w' => 'Width', 'h' => 'Height', 'wh' => 'Both');
  $element['property'] = array(
    '#title' => t('What to display'),
    '#type' => 'select',
    '#default_value' => $settings['property'],
    '#empty_option' => t('Nothing'),
    '#options' => $properties,
  );

  $element['property_separator'] = array(
    '#title' => t('Property separator'),
    '#type' => 'textfield',
    '#default_value' => $settings['property_separator'],
  );

  $element['property_suffix'] = array(
    '#title' => t('Property suffix'),
    '#type' => 'textfield',
    '#default_value' => $settings['property_suffix'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function image_wh_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $image_styles = image_style_options(FALSE);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  $property = '';
  if (isset($settings['property'])) {
    if ($settings['property'] == 'w') {
      $property = 'width';
    }
    if ($settings['property'] == 'h') {
      $property = 'height';
    }
    if ($settings['property'] == 'wh') {
      $property = 'width and height';
    }
  }

  if (isset($settings['property_suffix'])) {
    $suffix = t('with @suffix', array('@suffix' => $settings['property_suffix']));
  }

  if (isset($image_styles[$settings['image_style']])) {
    $summary[] = t('Displaying @property @suffix for image style: @style', array('@style' => $image_styles[$settings['image_style']], '@property' => $property, '@suffix' => $suffix));
  }
  else {
    $summary[] = t('Displaying @property @suffix for image', array('@suffix' => $suffix));
  }

  return implode('<br />', $summary);
}
/**
 * Implements hook_field_formatter_view().
 */
function image_wh_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'image_wh':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'image_wh_formatter',
          '#item' => $item,
          '#image_style' => $display['settings']['image_style'],
          '#property' => $display['settings']['property'],
          '#property_suffix' => $display['settings']['property_suffix'],
          '#property_separator' => $display['settings']['property_separator'],
        );
      }

      break;
  }

  return $element;
}

/**
 * Returns HTML for an image width and height field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An array of image data.
 *   - image_style: An optional image style.
 *   - property: Which properties to display ? (width, height or both).
 *   - property_separator: a string.
 *   - property_suffix: a string.
 *
 * @ingroup themeable
 */
function theme_image_wh_formatter($variables) {
  $item = $variables['item'];
  $image = $item['uri'];

  if (!empty($variables['image_style'])) {
    $image = image_style_path($variables['image_style'], $item['uri']);
  }

  $imagesize = getimagesize($image);
  $wh = array_values(array_slice($imagesize, 0, 2));

  if ($variables['property'] == 'w') {
    unset($wh['1']);
  }
  if ($variables['property'] == 'h') {
    unset($wh['0']);
  }

  $wh = array_map(function($value) use($variables) { return $value . $variables['property_suffix']; }, $wh);

  return implode($variables['property_separator'], $wh);
}
